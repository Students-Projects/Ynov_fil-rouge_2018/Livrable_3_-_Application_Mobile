package ynov.red_project.redwireproject;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Get_a_value extends Fragment{
    public static Get_a_value newInstance() {
        return (new Get_a_value());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.api_getter, container, false);
    }
}

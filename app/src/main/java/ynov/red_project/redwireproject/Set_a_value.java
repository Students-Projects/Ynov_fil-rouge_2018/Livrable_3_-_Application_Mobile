package ynov.red_project.redwireproject;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Set_a_value extends Fragment {
    public static Set_a_value newInstance() {
        return (new Set_a_value());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.api_setter, container, false);
    }
}

package ynov.red_project.redwireproject;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Switch;

public class PageAdapter extends FragmentPagerAdapter {
    //Default Constructor
    public PageAdapter(FragmentManager mgr) {
        super(mgr);
    }

    @Override
    public int getCount() {
        return(3);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: //Page number 1
                return Get_a_value.newInstance();
            case 1: //Page number 2
                return Set_a_value.newInstance();
            case 2: //Page number 3
                return informations.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: //Page number 1
                return "Get Value";
            case 1: //Page number 2
                return "Set Value";
            case 2: //Page number 3
                return "Informations";
            default:
                return null;
        }
    }
}

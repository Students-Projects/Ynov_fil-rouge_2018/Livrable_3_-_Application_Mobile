package ynov.red_project.redwireproject;

public class User
{
    private int id;
    private String username;


    public User(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return this.username;
    }
}

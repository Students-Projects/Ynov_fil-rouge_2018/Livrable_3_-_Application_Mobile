# Livrable 3 : Application Mobile

-Réaliser une application mobile multiplateforme (iOS et Android) 
permettant la réalisation de toutes les fonctionnalités de l'API créée 
dans la 2nde Livrable

-Explication du choix de procéder à un mode de développement 
hybride ou natif, et mettre à disposition l'ensemble des 
sources du projet.

-Bonus : Procèder à l'implémentation de tests unitaires.
